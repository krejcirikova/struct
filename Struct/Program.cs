


using System;

class Program
{
    // Struct - je datovy typ, ktery nam dovoluje groupovat vicero zakladnich promennych do jednoho celku.
    public struct Coordinates
    {
        public int x;
        public int y;
    }

    public static void Main(string[] args)
    {
        // muzeme s nim pracovat podobne jako s beznou promennou. K jednotlivym zakladnim vlastnostem pristupujeme pres "."
        Coordinates pointA;
        pointA.x = 10;
        pointA.y = 10;

        // muzeme pouzit take operator "new" (zname z random)
        var pointB = new Coordinates();
        pointB.x = 20;
        pointB.y = 20;

        // muzeme stejne jako u ostatnich promennych pouzit zkraceny zapis pro inicializaci
        var pointC = new Coordinates() 
        { 
            x = 30, 
            y = 30 
        };

        // stejne jako u zakladnich promennych, muzeme i z nasich custom datovych typu delat pole a seznamy
        var poleBodu = new Coordinates[3] 
        { 
            pointA, 
            pointB, 
            pointC 
        };
        var seznamBodu = poleBodu.ToList();
        
        // a nyni si ukazeme, jak se da vyuzit metod, ktere nabizi pole. Napr pro nejvetsi hodnotu. Pro porovnani ukazu seznam cisel
        var poleCisel = new int[3] 
        { 
            1, 
            2, 
            3 
        };
        Console.WriteLine($"nejvetsi z cisel je: {poleCisel.Max()}");

        // jenze v mem vlastnim typu "Coordinates" pocitac nevi, podle ceho ma maximalni bod urcit. Podle X? Podle Y?
        // Nastesti mu to muzeme explicitne rict. Zase - je vic zpusobu jak toho docilit, ale my si ukazeme nasledujici
        // (btw, koho by zajimalo vic, prosim: https://learn.microsoft.com/cs-cz/dotnet/standard/delegates-lambdas)
        
        Console.WriteLine($"nejvetsi z Xovych souradnic je: {poleBodu.Max(bod => bod.x)}");
        // tim, ze kazda polozka pole ma (musi mit) stejny typ, muzeme mu rict, podle jake vlastnosti se ma seradit.
        // souradnice X je integer, a s tim uz si metoda Max() hrave poradi.
        // abych toho docilil, musel jsem si uvnitr zavorek docasne pojmenovat priklad jedne hodnoty v poli. Zvolil jsem "bod", ale muzete si tam dat co chcete.
        // zkuste upravit predchozi zapis a misto "bod" tam dat treba "point". Funguje?

        // Aby se nam zapis dostal trochu pod kuzi, vypiste na dalsim radku nejvetsi Ypsilonovou souradnici z "poleBodu". Zkuste NEpouzivat ctrl+c/ctrl+v, rychleji si to zapamatujete.


    }
}
